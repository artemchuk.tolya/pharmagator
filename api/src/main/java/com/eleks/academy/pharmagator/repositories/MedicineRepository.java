package com.eleks.academy.pharmagator.repositories;

import com.eleks.academy.pharmagator.entities.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MedicineRepository extends JpaRepository<Medicine, Long> {

    Optional<Medicine> findByTitle(String title);

    @Query(value = "select * from medicines m where m.title IN (:range)", nativeQuery = true)
    List<Medicine> findAllCitiesInCityRange(@Param("range") List<String> range);
}
