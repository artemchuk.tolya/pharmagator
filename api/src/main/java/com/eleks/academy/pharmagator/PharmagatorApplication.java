package com.eleks.academy.pharmagator;

import com.eleks.academy.pharmagator.repositories.MedicineRepository;
import org.postgresql.core.Oid;
import org.postgresql.core.Utils;
import org.postgresql.geometric.PGbox;
import org.postgresql.geometric.PGpoint;
import org.postgresql.jdbc.UUIDArrayAssistant;
import org.postgresql.util.ByteConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.validation.constraints.Positive;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableScheduling
public class PharmagatorApplication {

    static Object[] vals;
    public static void main(String[] args) {
        ApplicationContext ac = SpringApplication.run(PharmagatorApplication.class, args);
        var a = ac.getBean(MedicineRepository.class);
        List<String> range = new ArrayList<>();
        range.add("Віракса 500 мг таблетки №14");
        range.add("3-Дінір 300 мг капсули №10");
        var res = a.findAllCitiesInCityRange(range);
        System.out.println(res);
    }

}
